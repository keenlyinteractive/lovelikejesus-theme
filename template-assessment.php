<?php
/**
 * Template Name: Assessment Template
 */
?>

<?php global $current_user;
      get_currentuserinfo();
      $S2 =  $current_user->user_firstname ;
?>



<style>
	.gftitletext .gfield_label::before {
    content: " <?php echo $S2 ;?> ";
}
</style>


<?php while (have_posts()) : the_post(); ?>
  <?php get_template_part('templates/page', 'header'); ?>
  <?php get_template_part('templates/content', 'page'); ?>
<?php endwhile; ?>
