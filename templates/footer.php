<footer class="content-info">
  <div class="container">
  	<div class="footer-logo-wrapper">
	  	<div class="footer-logo"></div>
  	</div>
	<?php
      if (has_nav_menu('footer-menu')) :
        wp_nav_menu(['theme_location' => 'footer-menu', 'menu_class' => 'footer-menu list-inline']);
      endif;
	?>
  	<div class="footer-copyrights">
  		<span>Love Like Jesus (LLJ Ministries)</span> &nbsp;<span>&copy; <?php echo date("Y") ?> </span>
	</div>
    <?php dynamic_sidebar('sidebar-footer'); ?>
  </div>
</footer>
