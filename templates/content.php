<article <?php post_class(); ?>>
	<div class="row">
		<div class="col-sm-5">
			<?php
			$thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'feature-list' );
			$featured_url = $thumb['0'];
			?>
			<div class="entry-image-wrapper">
				<div class="entry-image" style="background-image: url('<?php echo $featured_url; ?>')">
					<a href="<?php the_permalink(); ?>">
				        <span class="link-spanner"></span>
				    </a>
				</div>
			</div>
		</div>
		<div class="col-sm-7">
			<header>
			    <h2 class="entry-title"><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h2>
			    <span class="entry-author"><?php the_author_posts_link(); ?></span>
			</header>
			<div class="entry-summary">
			    <?php the_excerpt(); ?>
			</div>
			<div class="entry-read-more">
				<a href="<?php the_permalink(); ?>" class="btn btn-primary">read more</a>
			</div>
		</div>
	</div>
</article>
