<header class="banner navbar navbar-default navbar-static-top" role="banner">
  <div class="container">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
        <span class="sr-only"><?= __('Toggle navigation', 'sage'); ?></span>
        <span class="bars"></span>
      </button>
      <a class="navbar-brand" href="<?= esc_url(home_url('/')); ?>"></a>
    </div>

    <nav class="collapse navbar-collapse" role="navigation">  
      <?php
      if (has_nav_menu('primary_navigation')) :
        wp_nav_menu(['theme_location' => 'primary_navigation', 'walker' => new wp_bootstrap_navwalker(), 'menu_class' => 'nav navbar-nav']);
      endif;
      ?>
      <div class="social-menu">
        <ul class="list-unstyled list-inline">
          <li><a id="facebook" href="https://www.facebook.com/lljcommunity/" target="_blank"><i class="fa fa-facebook-official"></i></a></li>
          <li><a id="twitter" href="https://twitter.com/welovelikejesus" target="_blank"><i class="fa fa-twitter"></i></a></li>
        </ul>
      </div>
    </nav>
  </div>
</header>
