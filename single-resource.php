<?php while (have_posts()) : the_post(); ?>
	<?php
	$photo_id = get_post_meta( get_the_ID(), '_resource_photo_id', 1 );
	$photo = wp_get_attachment_image( $photo_id, 'feature-list' );
	$name = get_post_meta( get_the_ID(), '_resource_name', 1 );
	$figure = get_post_meta( get_the_ID(), '_resource_figure', 1 );
	$company = get_post_meta( get_the_ID(), '_resource_company', 1 );
	$logo_id = get_post_meta( get_the_ID(), '_resource_logo_id', 1 );
	$logo = wp_get_attachment_image( $logo_id, 'thumb' );
	$website = get_post_meta( get_the_ID(), '_resource_website', 1 );
	$facebook = get_post_meta( get_the_ID(), '_resource_facebook', 1 );
	$twitter = get_post_meta( get_the_ID(), '_resource_twitter', 1 );
	$linkedin = get_post_meta( get_the_ID(), '_resource_linkedin', 1 );
	$email = get_post_meta( get_the_ID(), '_resource_email', 1 );
	$video_1 = esc_url( get_post_meta( get_the_ID(), '_resource_video_1', 1 ) );
	$video_2 = esc_url( get_post_meta( get_the_ID(), '_resource_video_2', 1 ) );
	$rss = esc_url( get_post_meta( get_the_ID(), '_resource_rss', 1 ) );
	$rss_feed = do_shortcode( '[rssonpage rss="' . $rss . '" feeds=3 excerpt="summery true" target="_blank"]' );
	$resourcetitle = get_the_title();
	?>
	<div class="title-background">
		<h1 class="entry-title">Helpful Resource: <?php the_title(); ?></h1>
  </div>

	<main class="main">
		<article <?php post_class(); ?>>
			<div class="share">
				<span>share this resource</span>
				<div class="addthis_toolbox addthis_default_style addthis_32x32_style">
					<a class="addthis_button_facebook">
							<i class="fa fa-facebook"></i>
					</a>
					<a class="addthis_button_twitter">
							<i class="fa fa-twitter"></i>
					</a>
					<a class="addthis_button_compact">
							<i class="fa fa-share-alt"></i>
					</a>
				</div>
			</div>
			<div class="row">
				<div class="entry-content col-sm-8">
					<?php the_content(); ?>
					<?php if($video_1) { ?>
					<hr />
					<h2>Featured Videos</h2>
					<?php } ?>
					<div class="row">
						<div class="col-sm-6">
							<?php if($video_1) {
								echo '<div class="embed-responsive embed-responsive-16by9">' . wp_oembed_get( $video_1 ) . '</div>';
							} ?>
						</div>
						<div class="col-sm-6">
							<?php if($video_2) {
								echo '<div class="embed-responsive embed-responsive-16by9">' . wp_oembed_get( $video_2 ) . '</div>';
							} ?>
						</div>
					</div>
				</div>
				<div class="col-sm-4 resource-sidebar">
					<?php if($photo_id) {
						echo $photo;
					} ?>
					<div class="resource-social">
						<h4>Connect</h4>
						<ul class="list-inline">
							<?php if($facebook) {
								echo '<li><a href="' . $facebook . '" target="_blank"><i class="fa fa-facebook"></i></a></li>';
							} ?>
							<?php if($twitter) {
								echo '<li><a href="' . $twitter . '" target="_blank"><i class="fa fa-twitter"></i></a></li>';
							} ?>
							<?php if($linkedin) {
								echo '<li><a href="' . $linkedin . '" target="_blank"><i class="fa fa-linkedin"></i></a></li>';
							} ?>
							<?php if($email) {
								echo '<li><a href="mailto:' . $email . '" target="_top"><i class="fa fa-envelope"></i></a></li>';
							} ?>
						</ul>
						<?php if($website) {
							echo '<a href="' . $website . '" target="_blank" class="btn btn-primary">Visit Website</a>';
						} ?>
					</div>
					<?php if($rss) {
						echo '<h2>More By ' . $resourcetitle . '</h2><div class="resource-rss">' . $rss_feed . '</div>';
					} ?>
				</div>
			</div>
		</article>
	</main><!-- /.main -->

<?php endwhile; ?>
