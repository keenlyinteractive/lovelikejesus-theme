<?php while (have_posts()) : the_post(); ?>
	<?php
	$thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'feature-cover' );
	$featured_url = $thumb['0'];
	?>
	<div class="title-background" style="background-image: url('<?php echo $featured_url; ?>')">
		<h1 class="entry-title"><?php the_title(); ?></h1>
    </div>

	<main class="main">
		<article <?php post_class(); ?>>
			<div class="entry-content">
				<div class="share">
					<span>share this article</span>
					<div class="addthis_toolbox addthis_default_style addthis_32x32_style">
						<a class="addthis_button_facebook">
						    <i class="fa fa-facebook"></i>
						</a>
						<a class="addthis_button_twitter">
						    <i class="fa fa-twitter"></i>
						</a>
						<a class="addthis_button_compact">
						    <i class="fa fa-share-alt"></i>
						</a>
					</div>
				</div>
				<?php the_content(); ?>

				<hr />
				<p>Categories: <?php the_category(' / ') ?>
		    </div>

		    <?php comments_template('/templates/comments.php'); ?>
		</article>
	</main><!-- /.main -->
	<?php $twitter = get_the_author_meta('llj_user_twitterusername'); ?>
	<aside class="sidebar">
		<div class="author-sidebar">
			<?php $author_image = get_the_author_meta('llj_user_profilephoto_id'); ?>
			<div class="author-profile-photo"><?php echo wp_get_attachment_image( $author_image ); ?></div>
			<p class="author-title">About <?php the_author(); ?></p>
			<p class="author-bio"><?php the_author_meta('description'); ?></p>
			<div class="author-social">
				<h4>Connect with Author</h4>
				<?php
				$facebook = get_the_author_meta('llj_user_facebookurl');
				$twitter = get_the_author_meta('llj_user_twitterusername');
				$linkedin = get_the_author_meta('llj_user_linkedinurl');
				$email = get_the_author_meta('llj_user_contactemail');
				$youtube = get_the_author_meta('llj_user_youtube');
				$vimeo = get_the_author_meta('llj_user_vimeo');
				$website = get_the_author_meta('llj_user_website');
				?>
				<ul class="list-inline">
					<?php if($facebook) {
						echo '<li><a href="' . $facebook . '" target="_blank"><i class="fa fa-facebook"></i></a></li>';
					} ?>
					<?php if($twitter) {
						echo '<li><a href="https://twitter.com/' . $twitter . '" target="_blank"><i class="fa fa-twitter"></i></a></li>';
					} ?>
					<?php if($linkedin) {
						echo '<li><a href="' . $linkedin . '" target="_blank"><i class="fa fa-linkedin"></i></a></li>';
					} ?>
					<?php if($email) {
						echo '<li><a href="mailto:' . $email . '?Subject=LoveLikeJesus%20Contact" target="_top"><i class="fa fa-envelope"></i></a></li>';
					} ?>
					<?php if($youtube) {
						echo '<li><a href="' . $youtube . '" target="_blank"><i class="fa fa-youtube-play"></i></a></li>';
					} ?>
					<?php if($vimeo) {
						echo '<li><a href="' . $vimeo . '" target="_blank"><i class="fa fa-vimeo"></i></a></li>';
					} ?>
				</ul>
				<?php if($website) {
					echo '<a href="' . $website . '" target="_blank" class="btn btn-primary">Visit Website</a>';
				} ?>
			</div>
			<?php
				$authorslug = get_the_author_meta( 'user_login' );
			?>
			<h3>More By <?php the_author(); ?><span><a href="<?php bloginfo('url');?>/articles/author/<?php echo $authorslug;?>">(View All)</a></span></h3>
			<ul class="list-unstyled author-more">
				<?php
				$author = get_the_author_meta( 'ID' );
				$args = array( 'author' => $author, 'posts_per_page' => 3 );
				$myposts = get_posts( $args );
				foreach ( $myposts as $post ) : setup_postdata( $post ); ?>
					<li>
						<a href="<?php the_permalink(); ?>"><h4><?php the_title(); ?></h4></a>
						<div class="entry-summary"><?php echo excerpt(25); ?></div>
					</li>
				<?php endforeach;
				wp_reset_postdata();?>
			</ul>
			<h3>Related Resources</h3>
			<ul class="list-unstyled author-more">
				<?php
				$tags = wp_get_post_tags($post->ID);
				if ($tags) {
				$first_tag = $tags[($nextTagThumb+'1')]->term_id;
				$args=array(
				'tag__in' => array($first_tag),
				'post__not_in' => array($post->ID),
				'showposts'=>3,
				'caller_get_posts'=>1
				);
				$my_query = new WP_Query($args);
				if( $my_query->have_posts() ) {
				while ($my_query->have_posts()) : $my_query->the_post(); ?>
				<li>
					<a href="<?php the_permalink(); ?>"><h4><?php the_title(); ?></h4></a>
					<div class="entry-summary"><?php echo excerpt(25); ?></div>
					<a href="#" class="btn btn-primary btn-sm">Read More</a>
				</li>
				<?php endwhile; }
				wp_reset_query();
				}
				?>
			</ul>
		</div>
	</aside><!-- /.sidebar -->
<?php endwhile; ?>
