<?php $featured_item = llj_get_option( 'featured_item' ) ?>
<?php
if($featured_item == "post") { ?>
	<div class="featured">
		<?php
		$args = array( 'posts_per_page' => 1, 'category' => '68', 'orderby' => 'rand' );
		$myposts = get_posts( $args );
		foreach ( $myposts as $post ) : setup_postdata( $post ); ?>
			<?php
			$thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'feature-cover' );
			$featured_url = $thumb['0'];
			?>
			<article class="featured-article" style="background-image: url('<?php echo $featured_url; ?>')">
				<a href="<?php the_permalink(); ?>">
			        <span class="link-spanner"></span>
			    </a>
				<div class="featured-title">
					<h1 class="entry-title"><?php the_title(); ?></h1>
					<span class="entry-author"><?php the_author(); ?></span>
				</div>
			</article>
		<?php endforeach;
		wp_reset_postdata();?>
	</div>
<?php } elseif ($featured_item == "product") { ?>
	<div class="featured">
		<?php
		$bg_id = llj_get_option( 'featured_background_1_id' );
		$bg = wp_get_attachment_image_src( $bg_id, 'feature-cover' );
		$bg_url = $bg['0'];
		?>
		<div class="hero" style="background-image: url(<?php echo $bg_url ?>)">
			<div class="item-1">
				<h1><?php echo llj_get_option( 'featured_title' ) ?></h1>
				<p><?php echo llj_get_option( 'featured_content' ) ?></p>
				<p><a href="<?php echo llj_get_option( 'featured_link' ) ?>" class="btn btn-primary"><?php echo llj_get_option( 'featured_button' ) ?></a></p>
			</div>
			<div class="item-2">
				<a href="<?php echo llj_get_option( 'featured_link' ) ?>"><img src="<?php echo llj_get_option( 'featured_product_image' ) ?>"></a>
			</div>
		</div>
	</div>
<?php } elseif ($featured_item == "html") { ?>
	<div class="featured">
		<?php
		$bg_id_2 = llj_get_option( 'featured_background_2_id' );
		$bg_2 = wp_get_attachment_image_src( $bg_id_2, 'feature-cover' );
		$bg_url_2 = $bg_2['0'];
		?>
		<div class="hero" style="background-image: url(<?php echo $bg_url_2 ?>)">
			<?php echo llj_get_option( 'featured_html' ) ?>
		</div>
	</div>
<?php } ?>

<div class="featured-quote quote-bg row">
	<div class="col-sm-8 col-sm-offset-2">
		<?php echo llj_get_option( 'home_quote_1' ) ?>
	</div>
</div>

<!-- Action Bar-->
<div class="section actionbar">
    <h3>I'd like to improve relationships in:</h3>

    <div class="row">
        <div class="col-md-2">
            <a href="http://lovelikejesus.com/articles/category/marriage/"><img src="http://lovelikejesus.com/wp-content/uploads/2015/12/Endorphins-addiciton-for-romance.jpg"> My Marriage</a>
        </div>

        <div class="col-md-2">
            <a href="http://lovelikejesus.com/articles/category/family/"><img src="http://lovelikejesus.com/wp-content/uploads/2016/10/mother-daughter-blog.jpeg"> My Family</a>
        </div>

        <div class="col-md-2">
            <a href="http://lovelikejesus.com/articles/category/friendship/"><img src="http://lovelikejesus.com/wp-content/uploads/2015/11/friends-big-man-on-campus-popularity-love-like-jesus.jpg"> My Friendships</a>
        </div>

        <div class="col-md-2">
            <a href="http://lovelikejesus.com/articles/category/workplace/"><img src="http://lovelikejesus.com/wp-content/uploads/2016/05/Pure-in-heart-web-.jpeg"> My Workplace</a>
        </div>

        <div class="col-md-2">
            <a href="http://lovelikejesus.com/articles/category/neighborhood/"><img src="http://lovelikejesus.com/wp-content/uploads/2015/11/soccer.jpg"> My Neighborhood</a>
        </div>

        <div class="col-md-2">
            <a href="http://lovelikejesus.com/articles/category/world/"><img src="http://lovelikejesus.com/wp-content/uploads/2016/05/pursuing-love-web.jpeg"> My World</a>
        </div>
    </div>
</div>




<div class="article-list">
	<?php
	$args = array( 'posts_per_page' => 2, 'category' => '69' );
	$myposts = get_posts( $args );
	foreach ( $myposts as $post ) : setup_postdata( $post ); ?>
	<?php get_template_part('templates/content') ?>
	<?php endforeach;
	wp_reset_postdata();?>
</div>

<div class="divider-quote quote-bg row">
	<div class="col-sm-8 col-sm-offset-2">
		<?php echo llj_get_option( 'home_quote_2' ) ?>
	</div>
</div>

<div class="row">
	<div class="col-sm-5 col-sm-offset-7 col-md-4 col-md-offset-8 col-lg-3 col-lg-offset-9">
		<?php category_filter(); ?>
	</div>
</div>

<div class="article-list">
	<?php
	$args = array( 'posts_per_page' => 20, 'category__not_in' => array(69,68) );
	$myposts = get_posts( $args );
	foreach ( $myposts as $post ) : setup_postdata( $post ); ?>
	<?php get_template_part('templates/content') ?>
	<?php endforeach;
	wp_reset_postdata();?>

	<?php echo do_shortcode('[ajax_load_more post_type="post" post_format="standard" category__not_in="69,68" posts_per_page="10" container_type="div" offset="20"]'); ?>
</div>
