<?php
/**
 * Sage includes
 *
 * The $sage_includes array determines the code library included in your theme.
 * Add or remove files to the array as needed. Supports child theme overrides.
 *
 * Please note that missing files will produce a fatal error.
 *
 * @link https://github.com/roots/sage/pull/1042
 */
$sage_includes = [
  'lib/assets.php',  // Scripts and stylesheets
  'lib/extras.php',  // Custom functions
  'lib/setup.php',   // Theme setup
  'lib/titles.php',  // Page titles
  'lib/wrapper.php',  // Theme wrapper class
  'lib/wp_bootstrap_navwalker.php',  // Nav Walker
  'lib/custom_scripts.php'  // Custom Scripts
];

foreach ($sage_includes as $file) {
  if (!$filepath = locate_template($file)) {
    trigger_error(sprintf(__('Error locating %s for inclusion', 'sage'), $file), E_USER_ERROR);
  }

  require_once $filepath;
}
unset($file, $filepath);

// Image Sizes

add_image_size( 'feature-cover', 1170, 520, true );
add_image_size( 'feature-list', 445, 284, true );
add_image_size( 'resource-list', 600, 600, true );

update_option( 'thumbnail_size_w', 255 );
update_option( 'thumbnail_size_h', 255 );

// Add Menus

function register_menu() {
  register_nav_menu('footer-menu', __('Footer Menu'));
}
add_action('init', 'register_menu');

function excerpt($limit) {
      $excerpt = explode(' ', get_the_excerpt(), $limit);
      if (count($excerpt)>=$limit) {
        array_pop($excerpt);
        $excerpt = implode(" ",$excerpt).'...';
      } else {
        $excerpt = implode(" ",$excerpt);
      }
      $excerpt = preg_replace('`\[[^\]]*\]`','',$excerpt);
      return $excerpt;
    }

function content($limit) {
  $content = explode(' ', get_the_content(), $limit);
  if (count($content)>=$limit) {
    array_pop($content);
    $content = implode(" ",$content).'...';
  } else {
    $content = implode(" ",$content);
  }
  $content = preg_replace('/\[.+\]/','', $content);
  $content = apply_filters('the_content', $content);
  $content = str_replace(']]>', ']]&gt;', $content);
  return $content;
}

add_filter( 'get_the_archive_title', function ($title) {
  if ( is_category() ) {
    $title = single_cat_title( '', false );
  } elseif ( is_tag() ) {
    $title = single_tag_title( '', false );
  } elseif ( is_tax( 'resource-type' ) ) {
    $title = single_tag_title( 'Resource: ', false );
  } elseif ( is_post_type_archive( 'resource') ) {
    $title = post_type_archive_title( '', false );
  } elseif ( is_author() ) {
    $title = '<span class="vcard">' . get_the_author() . '</span>' ;
  }
    return $title;
});

function tags_filter() {
    $tax = 'resource-type';
    $terms = get_terms( $tax );
    $count = count( $terms );

    if ( $count > 0 ): ?>
        <select class="form-control resource-tags selectpicker" onchange="location = this.options[this.selectedIndex].value;">
          <option selected disabled>Filter Resources</option>
        <?php
        foreach ( $terms as $term ) {
            $term_link = get_term_link( $term, $tax );
            echo '<option value="' . $term_link . '">' . $term->name . '</option>';
        } ?>
        </select>
    <?php endif;
}

function wpa_cpt_tags( $query ) {
    if ( $query->is_tag() && $query->is_main_query() ) {
        $query->set( 'post_type', array( 'resource' ) );
    }
}
add_action( 'pre_get_posts', 'wpa_cpt_tags' );

function category_filter() {
    $tax = 'category';
    $terms = get_terms( $tax );
    $count = count( $terms );

    if ( $count > 0 ): ?>
        <select class="form-control categorys selectpicker" onchange="location = this.options[this.selectedIndex].value;">
          <option selected disabled>Filter Articles</option>
          <option value="<?php echo home_url() ?>">All Articles</option>
        <?php
        foreach ( $terms as $term ) {
            $term_link = get_term_link( $term, $tax );
            echo '<option value="' . $term_link . '">' . $term->name . '</option>';
        } ?>
        </select>
    <?php endif;
}

add_action( 'after_setup_theme', 'woocommerce_support' );
function woocommerce_support() {
    add_theme_support( 'woocommerce' );
}


// Bootstrap pagination function

function wp_bs_pagination($pages = '', $range = 4)
  {
   $showitems = ($range * 2) + 1;
     global $paged;
     if(empty($paged)) $paged = 1;
     if($pages == '')
     {
         global $wp_query;
		 $pages = $wp_query->max_num_pages;
         if(!$pages)
         {
             $pages = 1;
         }
     }
     if(1 != $pages)
     {
        echo '<div class="text-center">';
        echo '<nav><ul class="pagination"><li class="disabled hidden-xs"><span><span aria-hidden="true">Page '.$paged.' of '.$pages.'</span></span></li>';
         if($paged > 2 && $paged > $range+1 && $showitems < $pages) echo "<li><a href='".get_pagenum_link(1)."' aria-label='First'>&laquo;<span class='hidden-xs'> First</span></a></li>";
         if($paged > 1 && $showitems < $pages) echo "<li><a href='".get_pagenum_link($paged - 1)."' aria-label='Previous'>&lsaquo;<span class='hidden-xs'> Previous</span></a></li>";

         for ($i=1; $i <= $pages; $i++)
         {
             if (1 != $pages &&( !($i >= $paged+$range+1 || $i <= $paged-$range-1) || $pages <= $showitems ))
             {
                 echo ($paged == $i)? "<li class=\"active\"><span>".$i." <span class=\"sr-only\">(current)</span></span>
    </li>":"<li><a href='".get_pagenum_link($i)."'>".$i."</a></li>";
             }
         }
         if ($paged < $pages && $showitems < $pages) echo "<li><a href=\"".get_pagenum_link($paged + 1)."\"  aria-label='Next'><span class='hidden-xs'>Next </span>&rsaquo;</a></li>";
         if ($paged < $pages-1 &&  $paged+$range-1 < $pages && $showitems < $pages) echo "<li><a href='".get_pagenum_link($pages)."' aria-label='Last'><span class='hidden-xs'>Last </span>&raquo;</a></li>";
         echo "</ul></nav>";
         echo "</div>";
     }
}

function score_avg( $field_id, $entries ) {
  if ($entries[2][$field_id] != NULL) {
    $average = ($entries[0][$field_id] + $entries[1][$field_id] + $entries[2][$field_id]) / 3;
    return round($average,1);
  } elseif ($entries[2][$field_id] === NULL && $entries[1][$field_id] != NULL) {
    $average = ($entries[0][$field_id] + $entries[1][$field_id]) / 2;
    return round($average,1);
  } elseif ($entries[1][$field_id] === NULL) {
    return $entries[0][$field_id];
  }
}

function user_score( $field_id, $entries ) {
    return $entries[0][$field_id];
}
