<?php
/**
 * Template Name: Authors Template
 */
?>

<?php while (have_posts()) : the_post(); ?>
  <?php get_template_part('templates/page', 'header'); ?>
  <?php get_template_part('templates/content', 'page'); ?>
<?php endwhile; ?>

<?php
$authors = get_users( 'blog_id=1&orderby=lastname&role=contributor' );
// Array of WP_User objects.
foreach ( $authors as $user ) {
	$image_id = esc_html( $user->llj_user_profilephoto_id );
	$url_id = esc_html( $user->ID );
	$name = esc_html( $user->display_name );
	$image = wp_get_attachment_image( $image_id );
	$bio = $user->description;
	$url = get_author_posts_url( $url_id );
	echo '<div class="row author-list"><div class="col-sm-2"><a href="' . $url . '">' . $image . '</a></div><div class="col-sm-10"><a href="' . $url . '"><h2 class="author-title">' . $name . '</h2></a><p>' . $bio . '</p><a href="' . $url . '" class="btn btn-primary">read articles</a></div></div><hr/>';
}
