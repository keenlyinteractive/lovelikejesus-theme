<?php

add_action( 'wp_enqueue_scripts', 'llj_adding_styles', 100 );
function llj_adding_styles() {
	wp_register_style('font-awesome', ('https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css'));
	wp_enqueue_style('font-awesome');
}

function chart_script() {
	if ( is_page( 360 ) ) {
		wp_register_script('chart-script', get_template_directory_uri() . '/assets/scripts/Chart.min.js');
		wp_enqueue_script('chart-script');
	}
}
add_action( 'wp_enqueue_scripts', 'chart_script', 100 );
