<?php use Roots\Sage\Titles; ?>

<?php
$resource_bg_id = llj_get_option( 'resources_background_id' );
$resource_bg = wp_get_attachment_image_src( $resource_bg_id, 'feature-cover' );
$resource_bg_url = $resource_bg['0'];
?>
<div class="row">
  <div class="title-background" style="Background-image: url(<?php echo $resource_bg_url ?>)">
    <h1 class="entry-title"><?= Titles\title(); ?></h1>
  </div>
</div>

<div class="row resource-header">
  <div class="col-sm-12">
    <?php echo llj_get_option( 'resource_content' ) ?>
  </div>
  <div class="col-sm-5 col-sm-offset-7 col-md-4 col-md-offset-8 col-lg-3 col-lg-offset-9">
    <?php tags_filter(); ?>
  </div>
</div>

<?php if (!have_posts()) : ?>
  <div class="alert alert-warning">
    <?php _e('Sorry, no results were found.', 'sage'); ?>
  </div>
  <?php get_search_form(); ?>
<?php endif; ?>

<?php while (have_posts()) : the_post(); ?>
  <div class="article-list">
  	<article <?php post_class(); ?>>
  		<div class="row">
  			<div class="col-sm-3">
  				<?php
          $photo_id = get_post_meta( get_the_ID(), '_resource_photo_id', 1 );
  				$photo = wp_get_attachment_image_src( $photo_id, 'resource-list' );
  				$photo_url = $photo['0'];
  				?>
  				<div class="entry-image-wrapper">
  					<div class="entry-image" style="background-image: url('<?php echo $photo_url; ?>')">
  						<a href="<?php the_permalink(); ?>">
				        <span class="link-spanner"></span>
					    </a>
  					</div>
  				</div>
  			</div>
  			<div class="col-sm-9">
  				<header>
  				    <h2 class="entry-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
  				</header>
  				<div class="entry-summary">
  				    <?php the_excerpt(); ?>
  				</div>
  				<div class="entry-read-more">
  					<a href="<?php the_permalink(); ?>" class="btn btn-primary">learn more</a>
  				</div>
  			</div>
  		</div>
  	</article>
  </div>
<?php endwhile; ?>

<?php
if (function_exists("wp_bs_pagination"))
  {
    wp_bs_pagination();
  }
?>
