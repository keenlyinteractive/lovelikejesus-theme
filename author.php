<?php $author_image_id = get_the_author_meta('llj_user_coverphoto_id');
$author_image_url_array = wp_get_attachment_image_src($author_image_id, 'feature-cover');
$author_image_url = $author_image_url_array[0]; ?>
<div class="author-title-background" style="background-image: url('<?php echo $author_image_url; ?>')">
	<h1 class="entry-title"><?php the_author(); ?></h1>
</div>

<main class="main">
	<div class="author-article-list">
		<?php if (!have_posts()) : ?>
		  <div class="alert alert-warning">
		    <?php _e('Sorry, no results were found.', 'sage'); ?>
		  </div>
		  <?php get_search_form(); ?>
		<?php endif; ?>

		<?php while (have_posts()) : the_post(); ?>
			<article class="author-article">
				<?php
				$thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'feature-cover' );
				$featured_url = $thumb['0'];
				?>
				<div class="author-article-image-wrapper">
					<div class="author-article-image" style="background-image: url('<?php echo $featured_url; ?>')">
						<a href="<?php the_permalink(); ?>">
					        <span class="link-spanner"></span>
					    </a>
						<h2 class="entry-title"><?php the_title(); ?></h2>
					</div>
				</div>
				<div class="author-article-content">
					<time class="updated" datetime="<?= get_post_time('c', true); ?>"><?= get_the_date(); ?></time>,
					<span><?php the_author(); ?></span>
					<a href="<?php the_permalink(); ?>" class="btn btn-primary">read more</a>
				</div>
				<div class="clearfix"></div>
			</article>
		<?php endwhile; ?>

		<?php the_posts_navigation(); ?>
	</div>
</main>


<aside class="sidebar">
	<div class="author-sidebar">
		<p class="author-title">About Author</p>
		<p class="author-bio"><?php the_author_meta('description'); ?></p>
		<div class="author-social">
			<h4>Connect with Author</h4>
			<?php
			$facebook = get_the_author_meta('llj_user_facebookurl');
			$twitter = get_the_author_meta('llj_user_twitterusername');
			$linkedin = get_the_author_meta('llj_user_linkedinurl');
			$email = get_the_author_meta('llj_user_contactemail');
			$youtube = get_the_author_meta('llj_user_youtube');
			$vimeo = get_the_author_meta('llj_user_vimeo');
			$website = get_the_author_meta('llj_user_website');
			?>
			<ul class="list-inline">
				<?php if($facebook) {
					echo '<li><a href="' . $facebook . '" target="_blank"><i class="fa fa-facebook"></i></a></li>';
				} ?>
				<?php if($twitter) {
					echo '<li><a href="https://twitter.com/' . $twitter . '" target="_blank"><i class="fa fa-twitter"></i></a></li>';
				} ?>
				<?php if($linkedin) {
					echo '<li><a href="' . $linkedin . '" target="_blank"><i class="fa fa-linkedin"></i></a></li>';
				} ?>
				<?php if($email) {
					echo '<li><a href="mailto:' . $email . '?Subject=LoveLikeJesus%20Contact" target="_top"><i class="fa fa-envelope"></i></a></li>';
				} ?>
				<?php if($youtube) {
					echo '<li><a href="' . $youtube . '" target="_blank"><i class="fa fa-youtube-play"></i></a></li>';
				} ?>
				<?php if($vimeo) {
					echo '<li><a href="' . $vimeo . '" target="_blank"><i class="fa fa-vimeo"></i></a></li>';
				} ?>
				<?php if($website) {
					echo '<li><a href="' . $website . '" target="_blank"><i class="fa fa-external-link"></i></a></li>';
				} ?>
			</ul>
		</div>
		<h3 class="twitter-title">Latest Tweets</h3>
		<?php echo do_shortcode('[mintweet username="'.$twitter.'" count="5" type="user" retweets=“0” replies=“0”]');	?>
		<a href="https://twitter.com/<?php echo $twitter?>" class="btn btn-primary" target="_blank">connect on twitter</a>
	</div>
</aside><!-- /.sidebar -->
