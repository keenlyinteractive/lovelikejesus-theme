<?php
/**
 * My Account page
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/myaccount/my-account.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you (the theme developer).
 * will need to copy the new files to your theme to maintain compatibility. We try to do this.
 * as little as possible, but it does happen. When this occurs the version of the template file will.
 * be bumped and the readme will list any important changes.
 *
 * @see 	    http://docs.woothemes.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

wc_print_notices(); ?>




 <div class="panel-group" id="accordion">
  
  
  <div class="panel panel-default">
    <div class="panel-heading">
      <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion" href="#collapse1">
        My Info</a>
      </h4>
    </div>
    <div id="collapse1" class="panel-collapse collapse">
      <div class="panel-body">
	      
	      
	      <p class="myaccount_user">
	<?php
	printf(
		__( 'Hello <strong>%1$s</strong> (not %1$s? <a href="%2$s">Sign out</a>).', 'woocommerce' ) . ' ',
		$current_user->display_name,
		wc_get_endpoint_url( 'customer-logout', '', wc_get_page_permalink( 'myaccount' ) )
	);

	printf( __( 'From your account dashboard you can track how you are doing in your relationships via the Love Assessment, view your recent orders, manage your shipping and billing addresses and <a href="%s">edit your password and account details</a>.', 'woocommerce' ),
		wc_customer_edit_account_url()
	);
	?>
</p>
	      
      </div>
    </div>
  </div>
  
  
  
  <div class="panel panel-default">
    <div class="panel-heading">
      <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion" href="#collapse2">
        My Love Assessment</a>
      </h4>
    </div>
    <div id="collapse2" class="panel-collapse collapse in">
      <div class="panel-body">
	      
	      
<?php
	$user_ID = get_current_user_id();

	$search_criteria_a['field_filters'][] = array( 'key' => '17', 'value' => $user_ID );
	$entries_a = GFAPI::get_entries( 9, $search_criteria_a );

	$patient_a = user_score(4, $entries_a);
	$kind_a = user_score(9, $entries_a);
	$envy_a = user_score(8, $entries_a);
	$proud_a = user_score(7, $entries_a);
	$rude_a = user_score(6, $entries_a);
	$insist_a = user_score(5, $entries_a);
	$wrongs_a = user_score(11, $entries_a);
	$irritable_a = user_score(10, $entries_a);
	$evil_a = user_score(12, $entries_a);
	$truth_a = user_score(13, $entries_a);

	$a_array = array($patient_a, $kind_a, $envy_a, $proud_a, $rude_a, $insist_a, $wrongs_a, $irritable_a, $evil_a, $truth_a);

	$search_criteria_b['field_filters'][] = array( 'key' => '16', 'value' => $user_ID );
	$entries_b = GFAPI::get_entries( 8, $search_criteria_b );

	$patient_b = score_avg(4, $entries_b);
	$kind_b = score_avg(9, $entries_b);
	$envy_b = score_avg(8, $entries_b);
	$proud_b = score_avg(7, $entries_b);
	$rude_b = score_avg(6, $entries_b);
	$insist_b = score_avg(5, $entries_b);
	$wrongs_b = score_avg(11, $entries_b);
	$irritable_b = score_avg(10, $entries_b);
	$evil_b = score_avg(12, $entries_b);
	$truth_b = score_avg(13, $entries_b);

	$b_array = array($patient_b, $kind_b, $envy_b, $proud_b, $rude_b, $insist_b, $wrongs_b, $irritable_b, $evil_b, $truth_b);
?>

<div class="col-md-7">
<h2>Your Test Results</h2>
</div>
<div class="col-md-5">
	<a href="https://lovelikejesus.com/self-assessment"><button>Record How You are Doing</button></a> 
	
	
	    <link href="http://addtocalendar.com/atc/1.5/atc-style-blue.css" rel="stylesheet" type="text/css">
    <script type="text/javascript">(function () {
            if (window.addtocalendar)if(typeof window.addtocalendar.start == "function")return;
            if (window.ifaddtocalendar == undefined) { window.ifaddtocalendar = 1;
                var d = document, s = d.createElement('script'), g = 'getElementsByTagName';
                s.type = 'text/javascript';s.charset = 'UTF-8';s.async = true;
                s.src = ('https:' == window.location.protocol ? 'https' : 'http')+'://addtocalendar.com/atc/1.5/atc.min.js';
                var h = d[g]('body')[0];h.appendChild(s); }})();
    </script>

    <!-- 3. Place event data -->
    <span class="addtocalendar ">
        <var class="atc_event">
            <var class="atc_date_start">
            <?php 
				//1 Day = 24*60*60 = 86400
				echo date("d-m-Y", time()+86400); 
				?>
            </var>
            <var class="atc_date_end">
				<?php 
				//1 Day = 24*60*60 = 86400
				echo date("d-m-Y", time()+86400); 
				?>
			</var>
            <var class="atc_timezone">Europe/London</var>
            <var class="atc_title">Check in at lovelikejesus.com</var>
            <var class="atc_description">Check in and log how you are doing in your relationships. Use the link: http://lovelikejesus.com/self-assessment</var>
            <var class="atc_location">On the go</var>
            <var class="atc_organizer">Love Like Jesus</var>
            <var class="atc_organizer_email">info@lovelikejesus.com</var>
        </var>
    </span>
	
</div>	
<div style="max-height: 700px">
	<canvas id="myChart" width="1110" height="400"></canvas>
</div>

<script>
// Get the context of the canvas element we want to select
var ctx = document.getElementById("myChart").getContext("2d");

var options = {

		scaleOverride: true,

		// ** Required if scaleOverride is true **
    // Number - The number of steps in a hard coded scale
    scaleSteps: 10,
    // Number - The value jump in the hard coded scale
    scaleStepWidth: 1,
    // Number - The scale starting value
    scaleStartValue: 0,

    ///Boolean - Whether grid lines are shown across the chart
    scaleShowGridLines : true,

    //String - Colour of the grid lines
    scaleGridLineColor : "rgba(0,0,0,.05)",

    //Number - Width of the grid lines
    scaleGridLineWidth : 1,

    //Boolean - Whether to show horizontal lines (except X axis)
    scaleShowHorizontalLines: true,

    //Boolean - Whether to show vertical lines (except Y axis)
    scaleShowVerticalLines: true,

    //Boolean - Whether the line is curved between points
    bezierCurve : true,

    //Number - Tension of the bezier curve between points
    bezierCurveTension : 0.4,

    //Boolean - Whether to show a dot for each point
    pointDot : true,

    //Number - Radius of each point dot in pixels
    pointDotRadius : 4,

    //Number - Pixel width of point dot stroke
    pointDotStrokeWidth : 1,

    //Number - amount extra to add to the radius to cater for hit detection outside the drawn point
    pointHitDetectionRadius : 20,

    //Boolean - Whether to show a stroke for datasets
    datasetStroke : true,

    //Number - Pixel width of dataset stroke
    datasetStrokeWidth : 2,

    //Boolean - Whether to fill the dataset with a colour
    datasetFill : true,

		responsive: true,

		maintainAspectRatio: false,

    //String - A legend template
    legendTemplate : "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].strokeColor%>\"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>"

};

var data = {
  labels: ["is patient", "is kind", "does not envy or boast", "is not arrogant or proud", "is not rude", "does not insist on his own way", "keeps no records of wrongs", "is not irritable or resentful", "does not delight in evil", "rejoices in the truth"],
  datasets: [
    <?php if ($entries_b[0][4] != NULL) { ?>{
      label: "What Others Say",
      fillColor: "rgba(0,0,0,0.2)",
      strokeColor: "rgba(53,26,19,1)",
      pointColor: "rgba(53,26,19,1)",
      pointStrokeColor: "#fff",
      pointHighlightFill: "#fff",
      pointHighlightStroke: "rgba(53,26,19,1)",
      data: [<?php echo implode(', ', $b_array); ?>]
    }, <?php } ?>
    {
      label: "Your Results",
      fillColor: "rgba(75,132,115,0.2)",
      strokeColor: "rgba(75,132,115,1)",
      pointColor: "rgba(75,132,115,1)",
      pointStrokeColor: "#fff",
      pointHighlightFill: "#fff",
      pointHighlightStroke: "rgba(75,132,115,1)",
      data: [<?php echo implode(', ', $a_array); ?>]
    }
  ]
};

var myLineChart = new Chart(ctx).Line(data, options);

</script>

	</div>
    </div>
  </div>

 
   <div class="panel panel-default">
    <div class="panel-heading">
      <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion" href="#collapse3">
        Invite Friends to Evaluate Me</a>
      </h4>
    </div>
    <div id="collapse3" class="panel-collapse collapse">
      <div class="panel-body">
	      <?php echo do_shortcode('[gravityform id="10" title="true" description="false" ajax="true"]'); ?>
      </div>
    </div>
  </div>
      
 
  <div class="panel panel-default">
    <div class="panel-heading">
      <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion" href="#collapse4">
        My Orders</a>
      </h4>
    </div>
    <div id="collapse4" class="panel-collapse collapse">
      <div class="panel-body">
	      
	      <?php do_action( 'woocommerce_before_my_account' ); ?>

<?php wc_get_template( 'myaccount/my-downloads.php' ); ?>

<?php wc_get_template( 'myaccount/my-orders.php', array( 'order_count' => $order_count ) ); ?>

<?php wc_get_template( 'myaccount/my-address.php' ); ?>

<?php do_action( 'woocommerce_after_my_account' ); ?>
	      
	      
      </div>
    </div>
  </div>
  
</div> 

















